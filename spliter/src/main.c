/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/03/13 15:25:48
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Andy (gk), andy.y.li@foxconn.com
 *        Company:  Foxconn
 *
 * =====================================================================================
 */

#include "strSpliter.h"

void testSpliter()
{
    StrSpliter spliter = getStrSpliter();
    spliter->split(spliter,"a,b,c,d,*e",",");
    spliter->show(spliter);

    spliter->split(spliter, "how$$$$are$you?","$");
    spliter->show(spliter);

    for (int i = 0;i < spliter->length(spliter);++i){
        printf("field[%d]: %s\n", i, spliter->field(spliter, i));
    }
    spliter->free(spliter);
}

int main()
{
    testSpliter();
    return 0;
}

