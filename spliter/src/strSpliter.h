/*
 * =====================================================================================
 *
 *       Filename:  strSpliter.h
 *
 *    Description: Define the split API for the header.  
 *
 *        Version:  1.0
 *        Created:  12/04/13 10:44:56
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Andy (gk), andy.y.li@foxconn.com
 *        Company:  Foxconn
 *
 * =====================================================================================
 */

#ifndef _STR_SPLITER_H
#define _STR_SPLITER_H

#include <stdio.h>

typedef struct _StrSplit{
    // The fields of the struct.
    char **result;
    int len;

    // Define APIs for the spliter.
    char ** (*split)(struct _StrSplit *this,const char *str,const char *delimeter);
    int (*length)(struct _StrSplit *this);
    void (*show)(struct _StrSplit *this);
    char * (*field)(struct _StrSplit *this, int n);
    void (*free)(struct _StrSplit *this);
}StrSplit,*StrSpliter;

StrSpliter getStrSpliter();

#endif

